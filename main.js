const curseforge = require("mc-curseforge-api");
const path = require("path");
const fs = require("fs").promises;
const fsSync = require("fs");
const axios = require("axios");
const fetch = require('node-fetch');
const Progress = require("node-fetch-progress");
const cliProgress = require('cli-progress');
const {program} = require('commander');
const exampleModsConfig = require("./cmods.example.json");

class Main {
  constructor() {
    this.pathToConfig = "./cmods.json";
    this.pathToConfigLock = "./cmods-lock.json";
    this.pathToMods = "./mods";

    this.multibar = new cliProgress.MultiBar({
      format: ' {bar} {percentage}% | "{filename}" | {value}/{total}',
      hideCursor: true,
      barCompleteChar: '\u2588',
      barIncompleteChar: '\u2591',
      clearOnComplete: false,
      stopOnComplete: true
    }, cliProgress.Presets.shades_grey);
    this.setupCommands();
  }

  setupCommands() {
    program.version('0.0.1');

    program
        .command("init")
        .description("inits default cmods.json config")
        .action(async () => {
          if (!this.existsConfigFile()) {
            await fs.writeFile(this.pathToConfig, JSON.stringify(exampleModsConfig, null, 2));
            console.log("Created default cmods.json config");
          } else {
            console.log("Error: cmods.json already exists");
          }
        });

    program
        .command('install <modname>')
        .alias('i')
        .description("adds new mod")
        .action(name => this.addMod(name));
    program
        .command('fetch')
        .alias('f')
        .description("fetch mods from config (installs all required)")
        .action(() => this.run());

    program
        .command('remove <modname>')
        .alias('rm')
        .description("removes mod")
        .action(name => this.removeMod(name));

    program
        .command('update')
        .alias('up')
        .description("update mods")
        .action(() => {
          this.run();
        });

    program
        .command('clean')
        .alias('clear')
        .description("removes all in mods directory")
        .action(() => {
          console.log("Cleaning mods dir");
          this.cleanModsPath();
        });
    program.parse(process.argv);
  }

  async addMod(name) {
    let config = await this.loadConfig();

    await this.fetchModFile({name, version: "latest"}, config.minecraft);

    config.mods[name] = "latest";
    await this.flushConfig(config);
  }

  async removeMod(name) {
    let config = await this.loadConfig();

    let lock = await this.loadConfigLock();
    let filename = lock[name];
    await fs.unlink(path.join(this.pathToMods, filename));
    delete config.mods[name];
    await this.flushConfig(config);
  }

  async cleanModsPath() {
    let files = await fs.readdir(this.pathToMods);
    // console.log(files);
    for (let filename of files) {
      let filePath = path.join(this.pathToMods, filename);
      if ((await fs.stat(filePath)).isFile()) {
        await fs.unlink(filePath);
        console.log(`DELETED - ${filePath}`)
      } else {
        await fs.rmdir(filePath);
      }
    }
    if (this.existsConfigFileLock()) {
      await fs.unlink(this.pathToConfigLock);
    }
  }

  async run() {
    let config = await this.loadConfig();
    let minecraftVersion = config.minecraft;
    let modsList = this.getConfigModsList(config.mods);

    this.checkModsPath();

    console.log(`MinecraftVersion: ${minecraftVersion}, ModsCount ${modsList.length}`);
    for (let mod of modsList) {
      try {
        this.fetchModFile(mod, minecraftVersion);
      } catch (e) {
        console.error(e);
      }
    }
    // this.multibar.
    // console.log("Loading complete.");
  }

  checkModsPath() {
    if (!fsSync.existsSync(this.pathToMods)) {
      fsSync.mkdirSync(this.pathToMods);
    }
  }

  async fetchModFile(mod, minecraftVersion) {
    let addonID = await this.getAddonIDbyName(mod.name);
    let addonFiles = await this.getAddonFiles(addonID);
    let availableFiles = this.getMinecraftModFiles(addonFiles, minecraftVersion);
    let target = availableFiles.pop();
    //console.log(fileToDownload);
    //console.log(`Loading ${mod.name}:${mod.version} - ${fileToDownload.fileName}`);
    await this.downloadModFile(target.downloadUrl, target.fileName, target.fileLength);

    let lock = this.existsConfigFileLock() ? await this.loadConfigLock() : {};

    lock[mod.name] = target.fileName;
    this.flushConfigLock(lock);
    return target;
  }

  getMinecraftModFiles(addonFiles, minecraftVersion) {
    return addonFiles.filter(file => file.gameVersion.includes(minecraftVersion)).sort(this.sortBy("id", "ASC"));
  }

  async getAddonIDbyName(name) {
    return (await curseforge.getMod(name)).id;
  }

  async getAddonFiles(addonID) {
    return (await axios.get(`https://addons-ecs.forgesvc.net/api/v2/addon/${addonID}/files`)).data;
  }

  async loadConfig() {
    return JSON.parse((await fs.readFile(this.pathToConfig)).toString());
  }

  async flushConfig(config) {
    return await fs.writeFile(this.pathToConfig, JSON.stringify(config, null, 2))
  }

  async loadConfigLock() {
    return JSON.parse((await fs.readFile(this.pathToConfigLock)).toString());
  }

  async flushConfigLock(config) {
    return await fs.writeFile(this.pathToConfigLock, JSON.stringify(config, null, 2))
  }

  existsConfigFile() {
    return fsSync.existsSync(this.pathToConfig);
  }

  existsConfigFileLock() {
    return fsSync.existsSync(this.pathToConfigLock);
  }

  sortBy(field = "id", type = "ASC",) {
    return (a, b) => {
      return type === "ASC"
          ? a[field] - b[field]
          : b[field] - a[field];
    }
  }

  downloadModFile(url, filename, filesize) {
    return new Promise((resolve, reject) => {
      let pathToFile = path.join(this.pathToMods, filename);
      //console.log(url);

      // if (fsSync.existsSync(pathToFile)) {
      //   resolve(filename);
      //   return;
      // }
      const file = fsSync.createWriteStream(pathToFile);
      fetch(url)
          .then(response => {
            // const bar1 = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
            //bar1.start(filesize, 0);
            const bar = this.multibar.create(filesize, 0)
            const progress = new Progress(response, {throttle: 100})
            progress.on('progress', (p) => {
              //bar.increment();
              bar.update(p.done, {filename});
              // bar1.update(p.done);
            });

            response.body.pipe(file);
            response.body.on('end', () => {
              //bar1.stop();
              resolve(filename);
            });
            response.body.on("error", reject);
            file.on("error", reject);
          })
          .catch(reject);
    });

  }

  findModByVersion(version) {
    return modFile => {
      return modFile.id === version;
    }
  }

  getConfigModsList(mods) {
    let list = [];
    for (let name in mods) {
      list.push({name, version: mods[name]})
    }
    return list;
  }

}


new Main();
